<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProspectiveStudents;
use App\Http\Requests;

class PutDataController extends Controller
{
    //
    public function store(Request $request){

      // TEMPORARY CORS FIX
       header('Access-Control-Allow-Origin: *');
       header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
       header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); // allow certain headers

        try
        {
          $response = new \stdClass();
          $response->data = ProspectiveStudents::create($request->all())->toArray();
          $response->status = 201;
          return JSON_encode($response);
        }
        catch(Exception $e)
        {
          echo "Error: " . $sql . "<br>" . $conn->error;
        }


    }
}
