<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProspectiveStudents extends Model
{
    //
    protected $table = "ProspectiveStudents";
    protected $fillable = [
      'first',
      'last',
      'email',
      'address',
      'city',
      'state',
      'zip',
      'tel',
      'expectedEnrollment',
      'degreeLevel'
    ];

    protected $hidden = [
        'remember_token'
    ];
}
